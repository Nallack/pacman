import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;


public class ReflexAgent extends PacManAgent {

	public enum Mode {SEARCH, CHASE, FLEE };
	
	private PacMan pacman;
	
	Mode mode = Mode.SEARCH;
	Point destination = new Point(0,0);
	
	public ReflexAgent(PacMan pacman) {
		this.pacman = pacman;
	}
	
	public Point getDestination() { return destination; }
	
	public Color getColor() {
		if(mode == ReflexAgent.Mode.SEARCH) return new Color(255, 255, 0);
		if(mode == ReflexAgent.Mode.FLEE) return new Color(255, 0, 0);
		if(mode == ReflexAgent.Mode.CHASE) return new Color(0, 0, 255);
		else return null;
	}
	
	/**
	 * Called by the pacman, can choose amongst a number of algorithms
	 * @param maze
	 * @return
	 */
	public Orientation getNext(Maze maze) {
		
		//TODO remove to run faster, should work without this though
		//if(!pacman.atGrid()) return pacman.currentOrientation;
	
		return searchAndRetreat(maze);
	}
	
	/////////Heuristics/utilities/////////////
	/* Dot Chains
	 * searches out a number of grid squares away and determine the max
	 * amount of dots it can eat.
	 * will favor long paths of dots and avoid dead ends
	 */
	
	/* closest dot
	 * evaluate the closest dot and how many moves away it is
	 * can be acheived quickly with A*
	 */
	
	/* Ghost Distance
	 * summed distance pacman is from all of the ghosts.
	 */
	
	/* Killing Spree
	 * evaluates when to eat an energizer based on distance of all ghosts 
	 */
	
	/* Ghost Prediction
	 * ghosts can't turn around, determine the positions of ghosts and how
	 * many moves away they will be.
	 */
	
	/* Segmentation
	 * uses a utility that determines how segmented the dots are in the maze to
	 * allow for pacman to try and avoid dots being scattered over the maze.
	 */
	///////////////////////////////////////////
	
	//region reflex
	/**
	 * returns the direction that will lead to the closest dot or ghost
	 * if they are in panic mode.
	 * @param maze
	 * @return
	 */
	protected final Orientation searchAndRetreat(Maze maze) {
		
		final int CHASE_DIST = 10;
		final int FLEE_DIST = 5;
		final int FLEE_SEARCH_DIST = 10;
		
		
		Maze.Status[][] map = maze.getMap();
		Ghost[] ghosts = maze.getGhosts();
		
		//find closest ghost
		Point closestGhost = closestGhost(ghosts);
		int closestGhostDist = minDistance(pacman.currentPosition, closestGhost);
		
		boolean panic = ghosts[0].isPanic();
		boolean flee = closestGhostDist < MazeViewer.CELL_SIZE * FLEE_DIST;
		boolean chase = closestGhostDist < MazeViewer.CELL_SIZE * CHASE_DIST;
		
		if(!panic && flee) {
			mode = Mode.FLEE;
			//float d = summedGhostDistance(maze);
			//System.out.println(d);
			return utilityRunaway(maze, FLEE_SEARCH_DIST);
		}
		else if (panic && chase) {
			mode = Mode.CHASE;
			destination = closestGhost;
			return searchPointAS(closestGhost, maze);
		}
		mode = Mode.SEARCH;
		return searchDotBFS(maze);
	}
	
	//endregion
	
	// region utilities
	private Point closestGhost(Ghost[] ghosts) {
		Point closestGhost = new Point(0, 0);
		int closestGhostDist = Integer.MAX_VALUE;
		for (int i = 0; i < ghosts.length; i++) {
			if (!ghosts[i].isDead()) {
				int tempDist = Math.abs(pacman.currentPosition.x - ghosts[i].getPosition().x)
						     + Math.abs(pacman.getPosition().y - ghosts[i].getPosition().y);

				if (closestGhostDist > tempDist && !ghosts[i].isDead()) {
					closestGhostDist = tempDist;
					closestGhost = new Point(ghosts[i].getPosition().x, ghosts[i].getPosition().y);
				}
			}
		}
		return closestGhost;
	}
	
	private Orientation utilityRunaway(Maze maze, int maxDistance) {
		//find possible orientations
		Orientation[] possibleOrientations = getOrientations2(maze);
		Maze.Status[][] map = maze.getMap();
		int width = map.length;
		int height = map[0].length;
				
		Orientation ori = possibleOrientations[0];
		float lowest = Integer.MAX_VALUE;
		Point pm = screenToGrid(pacman.currentPosition, MazeViewer.CELL_SIZE);
		for(int i = 0; i < possibleOrientations.length; i++) {
			Point point = nextGridPos(pm, possibleOrientations[i], width, height);
			
			float ghostIDist = SIGhostDistances(point, maze, maxDistance);
			float dotIDist = INearestDotDistanceBFS(maze);
			
			float util = ghostIDist * 0.7f - dotIDist * 0.3f;
			
			//System.out.println(point + " : " + possibleOrientations[i] + " : " + dist);
			if(util < lowest) {
				lowest = util;
				ori = possibleOrientations[i];
			}
		}
		//return null;
		return ori;
	}
	
	//inverse nearest dot distance with ghost avoidance
	private float INearestDotDistanceBFS(Maze maze) {
		//calculations all done in grid space
		class Pair {
			Point point;
			int distance;
			
			public Pair(Point point, int distance) {
				this.point = point;
				this.distance = distance;
			}
		}
		Maze.Status[][] map = maze.getMap();

		int width = map.length;
		int height = map[0].length;
				
		Point pm = screenToGrid(pacman.currentPosition, MazeViewer.CELL_SIZE);
		
		Ghost[] ghosts = maze.getGhosts();
		Point[] g = new Point[ghosts.length];
		for(int i = 0; i < ghosts.length; i++) {
			g[i] = ghosts[i].getPosition();
		}
		
		Set<Point> visited = new HashSet<Point>();
		Queue<Pair> queue = new LinkedList<Pair>();
		queue.offer(new Pair(pm, 0));
		
		Point next = null;
		while(!queue.isEmpty())
		{
			Pair node = queue.poll();
			Point point = node.point;
			int distance = node.distance;
			
			for(Point ghost : g) {
				if(point.equals(ghost)) continue;
			}
			
			if(map[point.x][point.y] == Maze.Status.DOT) {
				destination = gridToScreen(point, MazeViewer.CELL_SIZE);
				return 100.0f / distance;
			}
				
			visited.add(point);
				
			//right
			next = new Point(wrap(point.x+1, width), point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
				
			//left
			next = new Point(wrap(point.x-1, width),point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//down
			next = new Point(point.x, wrap(point.y+1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//up
			next = new Point(point.x, wrap(point.y-1, height));
				if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
					queue.offer(new Pair(next, distance + 1));
			}
		}
		return 0;
	}
	
	//Sum of inverse ghost distances
	private float SIGhostDistances(Point from, Maze maze, int maxDistance) {
		class Pair {
			Point point;
			int distance;
			
			public Pair(Point point, int distance) {
				this.point = point;
				this.distance = distance;
			}
		}
		
		Maze.Status[][] map = maze.getMap();

		int width = map.length;
		int height = map[0].length;
		
		Ghost[] ghosts = maze.getGhosts();
		List<Point> ghostPositions = new ArrayList<Point>();
		for(Ghost ghost : ghosts) {
			ghostPositions.add(screenToGrid(ghost.getPosition(), MazeViewer.CELL_SIZE));
		}
		
		float total = 0;
		Set<Point> visited = new HashSet<Point>();
		Queue<Pair> queue = new LinkedList<Pair>();
		
		queue.offer(new Pair(from, 1));
		
		Point next = null;
		while(!queue.isEmpty())
		{
			Pair node = queue.poll();
			Point point = node.point;
			int distance = node.distance;
			
			for(int i = 0; i < ghostPositions.size(); i++) {
				if(ghostPositions.remove(point)) {
					total += 100.0f / distance;
				}
			}
			
			if(ghostPositions.isEmpty()) {
				return total;
			}
			
			visited.add(point);
			
			if(distance >= maxDistance) continue;
			
			//right
			next = new Point(wrap(point.x+1, width), point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//left
			next = new Point(wrap(point.x-1, width),point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//down
			next = new Point(point.x, wrap(point.y+1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//up
			next = new Point(point.x, wrap(point.y-1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
		}
		return total;
	}

	protected Orientation[] getOrientations2(Maze maze) {
		ArrayList<Orientation> list = new ArrayList<Orientation>(4);

		if (!(maze.locationStatus(PacMan.nextPos(pacman.currentPosition,
				Orientation.UP)) == Maze.Status.DEAD))
			list.add(Orientation.UP);

		if (!(maze.locationStatus(PacMan.nextPos(pacman.currentPosition,
				Orientation.DOWN)) == Maze.Status.DEAD))
			list.add(Orientation.DOWN);

		if (!(maze.locationStatus(PacMan.nextPos(pacman.currentPosition,
				Orientation.LEFT)) == Maze.Status.DEAD))
			list.add(Orientation.LEFT);

		if (!(maze.locationStatus(PacMan.nextPos(pacman.currentPosition,
				Orientation.RIGHT)) == Maze.Status.DEAD))
			list.add(Orientation.RIGHT);

		///System.out.println(java.util.Arrays.toString(list.toArray()));
		
		return list.toArray(new Orientation[0]);
	}
	
	//does a single search for which direction has the nearest
	//dot by tracing the serach path.
	protected Orientation searchDotBFS(Maze maze) {
		//calculations all done in grid space
		class Pair {
			Point point;
			Orientation ori;
			
			public Pair(Point point, Orientation ori) {
				this.point = point;
				this.ori = ori;
			}
		}
		Maze.Status[][] map = maze.getMap();
		Orientation[] possibleOrientations = getOrientations2(maze);

		int width = map.length;
		int height = map[0].length;
		
		Point pm = screenToGrid(pacman.currentPosition, MazeViewer.CELL_SIZE);
		
		if(map[pm.x][pm.y] == Maze.Status.DOT) {
			return pacman.currentOrientation;
		}
		
		Set<Point> visited = new HashSet<Point>();
		Queue<Pair> queue = new LinkedList<Pair>();
		
		for(Orientation ori : possibleOrientations) {
			Point point = nextGridPos(pm, ori, width, height);
			queue.offer(new Pair(point, ori));
		}
		
		Point next = null;
		while(!queue.isEmpty())
		{
			Pair node = queue.poll();
			Orientation ori = node.ori;
			Point point = node.point;
						
			if(map[point.x][point.y] == Maze.Status.DOT) {
				destination = gridToScreen(point, MazeViewer.CELL_SIZE);
				return ori;
			}
			
			visited.add(point);
			
			//right
			next = new Point(wrap(point.x+1, width), point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, ori));
			}
			
			//left
			next = new Point(wrap(point.x-1, width),point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, ori));
			}
			
			//down
			next = new Point(point.x, wrap(point.y+1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, ori));
			}
			
			//up
			next = new Point(point.x, wrap(point.y-1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, ori));
			}
		}
		return pacman.currentOrientation;
	}
	
	
	protected Orientation searchPointAS(Point targetPosition, Maze maze) {
		
		class Triple implements Comparable<Triple> {
			Point point;
			Orientation ori;
			int priority;
			
			public Triple(Point point, Orientation ori, int priority) {
				this.point = point;
				this.ori = ori;
				this.priority = priority;
			}

			@Override
			public int compareTo(Triple arg0) {
				return  priority - arg0.priority;
			}
		}
		
		Maze.Status[][] map = maze.getMap();
		Orientation[] possibleOrientations = getOrientations2(maze);

		int width = map.length;
		int height = map[0].length;	
		
		Point pm = screenToGrid(pacman.currentPosition, MazeViewer.CELL_SIZE);
		Point tg = screenToGrid(targetPosition, MazeViewer.CELL_SIZE);
		
		if(pm.equals(tg)) {
			return pacman.currentOrientation;
		}
		
		Set<Point> visited = new HashSet<Point>();
		PriorityQueue<Triple> pqueue = new PriorityQueue<Triple>();
		for(Orientation ori : possibleOrientations) {
			Point point = nextGridPos(pm, ori, width, height);
			pqueue.offer(new Triple(point, ori, minDistance(point, tg)));
		}
		
		Point next = null;
		while(!pqueue.isEmpty())
		{
			Triple node = pqueue.poll();
			int dist = node.priority;
			Orientation ori = node.ori;
			Point point = node.point;
			
			if(point.equals(tg)) {
				destination = gridToScreen(point, MazeViewer.CELL_SIZE);
				return ori;
			}
			visited.add(point);
			
			//right
			next = new Point(wrap(point.x+1, width), point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				pqueue.offer(new Triple(next, ori,  dist + minDistance(point, tg)));
			}
			
			//left
			next = new Point(wrap(point.x-1, width),point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				pqueue.offer(new Triple(next, ori, dist + minDistance(point, tg)));
			}
			
			//down
			next = new Point(point.x, wrap(point.y+1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				pqueue.offer(new Triple(next, ori,  dist + minDistance(point, tg)));
			}
			
			//up
			next = new Point(point.x, wrap(point.y-1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				pqueue.offer(new Triple(next, ori,  dist + minDistance(point, tg)));
			}
		}
		return pacman.getOrientation();
	}

	/**
	 * returns the next wrapped grid point
	 */
	protected Point nextGridPos(Point point, Orientation ori, int width, int height) {
		switch(ori) {
		case UP:
			return new Point(point.x, wrap(point.y - 1, height));
		case DOWN:
			return new Point(point.x, wrap(point.y + 1, height));
		case RIGHT:
			return new Point(wrap(point.x + 1, width), point.y);
		case LEFT:
			return new Point(wrap(point.x - 1, width), point.y);
		}
		return null;
	}

	

	//endregion utilities
}
