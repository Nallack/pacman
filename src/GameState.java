import java.awt.Point;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
	
public class GameState {
	
	Maze.Status[][] map;
	Point pacman;
	GhostState[] ghosts;
	
	int score;
	float panicCounter;

	public class GhostState {
		Point position;
		Orientation orientation;
		boolean isDead;
		
		public GhostState(Point position, Orientation orientation, boolean isDead) {
			this.position = position;
			this.orientation = orientation;
			this.isDead = isDead;
		}
		
		public GhostState clone() {
			return new GhostState((Point)position.clone(), orientation, isDead);
		}
	}
	
	private GameState() {
	}
	
	public GameState(Maze maze, int gridSize) {
		Maze.Status[][] currentMap = maze.getMap();
		
		this.map = new Maze.Status[currentMap.length][currentMap[0].length];
		for(int x = 0; x < map.length; x++) {
			for(int y = 0; y < map[0].length; y++) {
				map[x][y] = currentMap[x][y];
			}
		}
		
		this.pacman = ReflexAgent.screenToGrid(maze.getPacMan().getPosition(), gridSize);

		Ghost[] g = maze.getGhosts();
		this.ghosts = new GhostState[g.length];
		for(int i = 0; i < g.length; i++) {
			ghosts[i] = new GhostState(
					ReflexAgent.screenToGrid(g[i].getPosition(), gridSize),
					g[i].getOrientation(),
					g[i].isDead());
			
		}
		
		this.score = maze.getScore();
		
		if(maze.getGhosts()[0].isPanic()) this.panicCounter = 7.0f;
		else this.panicCounter = 0.0f;
	}
	
	public Orientation[] getPacManMoves() {
		boolean up = false;
		boolean down = false;
		boolean left = false;
		boolean right = false;
		
		int x = pacman.x;
		int y = pacman.y;
		
		int counter = 0;
		if(map[x][y-1] != Maze.Status.DEAD) {
			up = true;
			counter++;
		}
		if(map[x][y+1] != Maze.Status.DEAD) {
			down = true;
			counter++;
		}
		if(map[PacManAgent.wrap(x-1, map.length)][y] != Maze.Status.DEAD) {
			left = true;
			counter++;
		}
		if(map[PacManAgent.wrap(x+1, map.length)][y] != Maze.Status.DEAD) {
			right = true;
			counter++;
		}
		
		Orientation[] oris = new Orientation[counter];
		counter = 0;
		if(up) oris[counter++] = Orientation.UP;
		if(down) oris[counter++] = Orientation.DOWN;
		if(left) oris[counter++] = Orientation.LEFT;
		if(right) oris[counter++] = Orientation.RIGHT;
		
		return oris;
	}
	
	public void pacmanMove(Orientation ori) {
		//have to remove dot after pacman is done with a square,
		//otherwise he will evaluate a future state where there is no dot
		//there, only a slightly higher score
				
		if(map[pacman.x][pacman.y] == Maze.Status.DOT) {
			map[pacman.x][pacman.y] = Maze.Status.LEGAL;
			score += 10;
		}
		else if(map[pacman.x][pacman.y] == Maze.Status.ENERGISER) {
			map[pacman.x][pacman.y] = Maze.Status.LEGAL;
			panicCounter = 7.0f;
			score += 50;
		}
		
		switch(ori) {
		case UP:
			pacman.y = pacman.y - 1;
			break;
		case DOWN:
			pacman.y = pacman.y + 1;
			break;
		case LEFT:
			pacman.x = PacManAgent.wrap(pacman.x - 1, map.length);
			break;
		case RIGHT:
			pacman.x = PacManAgent.wrap(pacman.x + 1, map.length);
			break;
		}
		
		for(GhostState ghost : ghosts) {
			if(ghost.position.equals(pacman)) {
				ghost.isDead = true;
				score += 1000;
			}
		}
		
		if(panicCounter > 0.0f) panicCounter-= 0.2f;
	}
	
	public void ghostMove(int ghost, Orientation ori) {
		switch(ori) {
		case UP:
			ghosts[ghost].position.y = ghosts[ghost].position.y - 1;
			break;
		case DOWN:
			ghosts[ghost].position.y = ghosts[ghost].position.y + 1;
			break;
		case LEFT:
			ghosts[ghost].position.x = PacManAgent.wrap(ghosts[ghost].position.x - 1, map.length);
			break;
		case RIGHT:
			ghosts[ghost].position.x = PacManAgent.wrap(ghosts[ghost].position.x + 1, map.length);
			break;
		}
		
		if(panicCounter > 0.0f) panicCounter-= 0.2f;
	}
	
	public GameState clone() {
		GameState clone = new GameState();
		
		clone.map = new Maze.Status[map.length][map[0].length];
		for(int x = 0; x < map.length; x++) {
			for(int y = 0; y < map[0].length; y++) {
				clone.map[x][y] = map[x][y];
			}
		}
		
		clone.pacman = (Point)pacman.clone();
		clone.ghosts = new GhostState[ghosts.length];
		
		for(int i = 0; i < ghosts.length; i++) clone.ghosts[i] = ghosts[i].clone();
		
		clone.score = score;
		clone.panicCounter = panicCounter;
		
		return clone;
	}
	
	public Orientation[] getGhostMoves(int ghost) {
		boolean up = false;
		boolean down = false;
		boolean left = false;
		boolean right = false;
		
		int x = ghosts[ghost].position.x;
		int y = ghosts[ghost].position.y;
		
		int counter = 0;
		if(ghosts[ghost].orientation != Orientation.UP && map[x][y-1] != Maze.Status.DEAD) {
			up = true;
			counter++;
		}
		if(ghosts[ghost].orientation != Orientation.DOWN && map[x][y+1] != Maze.Status.DEAD) {
			down = true;
			counter++;
		}
		if(ghosts[ghost].orientation != Orientation.LEFT && map[PacManAgent.wrap(x-1, map.length)][y] != Maze.Status.DEAD) {
			left = true;
			counter++;
		}
		if(ghosts[ghost].orientation != Orientation.RIGHT && map[PacManAgent.wrap(x+1, map.length)][y] != Maze.Status.DEAD) {
			right = true;
			counter++;
		}
		
		Orientation[] oris = new Orientation[counter];
		counter = 0;
		if(up) oris[counter++] = Orientation.UP;
		if(down) oris[counter++] = Orientation.DOWN;
		if(left) oris[counter++] = Orientation.LEFT;
		if(right) oris[counter++] = Orientation.RIGHT;
		
		return oris;
	}

	//we dont care which ghost moved where, we just want
	//all the possible combinations ghosts can move
	public GameState[] getGhostStates() {
		List<GameState> states = new LinkedList<GameState>();
		
		Orientation[][] orientations = new Orientation[ghosts.length][];
		for(int i = 0; i < ghosts.length; i++) {
			orientations[i] = getGhostMoves(i);
		}
		
		//TODO DEBUG
		//for(int i = 0; i < 4; i++) {
		//	System.out.println(Arrays.toString(orientations[i]));
		//}
		
		for(int i = 0; i < orientations[0].length; i++) {
			for(int j = 0; j < orientations[1].length; j++) {
				for(int k = 0; k < orientations[2].length; k++) {
					for(int l = 0; l < orientations[3].length; l++) {
						GameState clone = this.clone();
						clone.ghostMove(0, orientations[0][i]);
						clone.ghostMove(1, orientations[1][j]);
						clone.ghostMove(2, orientations[2][k]);
						clone.ghostMove(3, orientations[3][l]);
						states.add(clone);
					}
				}
			}
		}
		
		return states.toArray(new GameState[0]);
	}
	
	public String toString() {
		char[][] grid = new char[map[0].length][map.length];
		
		for(int x = 0; x < map.length; x++) {
			for(int y = 0; y < map[0].length; y++) {
				switch(map[x][y]) {
				case LEGAL:
					grid[y][x] = '+';
					break;
				case INVALID:
					grid[y][x] = 'x';
					break;
				case DOT:
					grid[y][x] = '.';
					break;
				case ENERGISER:
					grid[y][x] = 'o';
					break;
				case DEAD:
					grid[y][x] = ' ';
					break;
				}
			}
		}
		
		grid[pacman.y][pacman.x] = 'p';
		for(GhostState ghost : ghosts) grid[ghost.position.y][ghost.position.x] = 'g';
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < grid.length; i++) {
			sb.append(grid[i]);
			sb.append('\n');
		}
		
		return sb.toString();
	}
}