import java.awt.Color;
import java.awt.Point;

public abstract class PacManAgent {
	
	public abstract Point getDestination();
	public abstract Color getColor();
	public abstract Orientation getNext(Maze maze);
	
	public int minDistance(Point a, Point b) {
		return Math.abs(b.x - a.x) + Math.abs(b.y - a.y);
	}

	public static Point screenToGrid(Point point, int gridSize) {
		return new Point(point.x / MazeViewer.CELL_SIZE, point.y / MazeViewer.CELL_SIZE);
	}
	
	public static Point gridToScreen(Point point, int gridSize) {
		return new Point(point.x * MazeViewer.CELL_SIZE, point.y * MazeViewer.CELL_SIZE);
	}
	
	public static int wrap(int value, int modulus) {
		if(value >= 0) return value % modulus;
		else return (modulus + value % modulus) % modulus;
	}
}
