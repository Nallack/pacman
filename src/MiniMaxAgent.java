import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.PriorityQueue;


public class MiniMaxAgent extends PacManAgent {

	PacMan pacman;
	Point destination = new Point(0,0);
	int[][][][] distances = new int[28][36][28][36]; //gives the actual (BFS) distance to the dot at [x][y][][] for pacman at [][][x][y].
	
	public MiniMaxAgent(PacMan pacman) {
		this.pacman = pacman;
	}
	
	public MiniMaxAgent(PacMan pacman, GameState initialState) {
		this.pacman = pacman;
		distances = getDistances(initialState);
	}
	
	@Override
	public Orientation getNext(Maze maze) {
		
		//TODO remove to run faster, should work without this though
		if(!pacman.atGrid()) return pacman.currentOrientation;
		GameState currentState = new GameState(maze, MazeViewer.CELL_SIZE);
		return runMinMax(currentState, 4);
	}

	
	public Point getDestination() {
		return destination;
	}

	@Override
	public Color getColor() {
		return new Color(255, 255, 0);
	}

	//region minimax
	public Orientation runMinMax(GameState currentState, int depth) {
		Orientation bestMove = pacman.currentOrientation;
		float best = Float.NEGATIVE_INFINITY;
		
		for(Orientation testMove : currentState.getPacManMoves()) {
			GameState testState = currentState.clone();
			testState.pacmanMove(testMove);
			
			float utility = minMax(testState, depth-1, Float.NEGATIVE_INFINITY, Float.POSITIVE_INFINITY, false);
			//if(testMove == Orientation.LEFT) System.out.print("LEFT: ");
			//if(testMove == Orientation.RIGHT) System.out.print("RIGHT: ");
			//if(testMove == Orientation.UP) System.out.print("UP: ");
			//if(testMove == Orientation.DOWN) System.out.print("DOWN: ");
			//System.out.println(utility);
			
			if(utility > best){
				bestMove = testMove;
				best = utility;
			}
		}
		
		return bestMove;
	}
	
	private float minMax(GameState currentState, int depth, float alpha, float beta, boolean maximizing) {
		
		if(depth == 0 || isTerminal(currentState)){
			return utility(currentState);
		}
		if(maximizing) {
			float best = Float.NEGATIVE_INFINITY;
			for(Orientation testMove : currentState.getPacManMoves()){
				GameState testState = currentState.clone();
				testState.pacmanMove(testMove);
				float n = minMax(testState, depth-1, alpha, beta, false);
				if(n > best) best = n;
				if(best > alpha) alpha = best;
				if(beta <= alpha) break;
			}
			return best;
		}
		else {
			float best = Float.POSITIVE_INFINITY;
			for(GameState testState : currentState.getGhostStates()){
				float n = minMax(testState, depth-1, alpha, beta, true);
				if(n < best) best = n;
				if(best < beta) beta = best;
				if(beta <= alpha) break;
			}
			return best;
		}
	}

	private float utility(GameState state) {
		
		for(GameState.GhostState ghost : state.ghosts) {
			if(state.pacman.equals(ghost.position) && (state.panicCounter <= 0)){
				return Float.NEGATIVE_INFINITY;
			}
		}
		Maze.Status[][] map = state.map;
		Point pm = state.pacman;
		float dotsLeft = getRemainingDots(state);
		float SIDghost = SIDGhostBFS(state, 10);
		float wdd = weightedDotDensity(state);
		float utility = 0;
		if(state.panicCounter > 0) utility = 246.0f - dotsLeft + 0.01f*wdd + 100.0f*SIDghost;
		else utility = 246.0f - dotsLeft + 0.01f*wdd - 10.0f*SIDghost;
		return utility;
	}
	
	private boolean isTerminal(GameState state) {
		for(GameState.GhostState ghost : state.ghosts)
		if(state.pacman.equals(ghost.position) && state.panicCounter > 0.0f) return true;
		return false;
	}
	
	//endregion
	
	//region helper
	//inverse nearest dot distance with ghost avoidance
	//High is good
	private float NearestDotDistanceBFS(GameState state) {
		class Pair {
			Point point;
			int distance;
			
			public Pair(Point point, int distance) {
				this.point = point;
				this.distance = distance;
			}
		}
		Maze.Status[][] map = state.map;
		int width = map.length;
		int height = map[0].length;
				
		Point pm = state.pacman;		
		
		GameState.GhostState[] ghosts = state.ghosts;
		
		Set<Point> visited = new HashSet<Point>();
		Queue<Pair> queue = new LinkedList<Pair>();
		queue.offer(new Pair(pm, 1));
		
		Point next = null;
		while(!queue.isEmpty())
		{
			Pair node = queue.poll();
			Point point = node.point;
			int distance = node.distance;
			
			for(GameState.GhostState ghost : ghosts) {
				if(point.equals(ghost.position)) continue;
			}
			
			
			if(map[point.x][point.y] == Maze.Status.DOT) {
				return distance;
			}
		
			visited.add(point);
					
			//right
			next = new Point(wrap(point.x+1, width), point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//left
			next = new Point(wrap(point.x-1, width),point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
				
			//down
			next = new Point(point.x, wrap(point.y+1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//up
			next = new Point(point.x, wrap(point.y-1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
		}
		return 0;
	}
	
	private float getRemainingDots(GameState state){
		Maze.Status[][] map = state.map;
		int width = map.length;
		int height = map[0].length;
		float remainingDots = 0;
		
		for(int i = 0; i < width; i++){
			for(int j = 0; j < height; j++){
				if((map[i][j] == Maze.Status.DOT) || (map[i][j] == Maze.Status.ENERGISER)) remainingDots++;
			}
		}
		return (float) remainingDots;
	}
	
	private float weightedDotDensity(GameState state){
		Maze.Status[][] map = state.map;
		int width = map.length;
		int height = map[0].length;
		float weightedDotDensity = 0;
				
		Point pm = state.pacman;
		for(int i = pm.x-28; i < pm.x+28; i++){
			for(int j = pm.y-36; j < pm.y+36; j++){
				if((i < 0) || (i > 27) || (j < 0) || (j > 35)){
					continue;
				}
				else if((i == pm.x) && (j == pm.y)){
					if((map[i][j] == Maze.Status.DOT) || (map[i][j] == Maze.Status.ENERGISER)){
						weightedDotDensity+=2;
					}
				}
				else if((map[i][j] == Maze.Status.DOT) || (map[i][j] == Maze.Status.ENERGISER)){
					weightedDotDensity+= ((float) i+j+1)/((float) distances[i][j][pm.x][pm.y]);
				}
			}
		}
		return weightedDotDensity;
	}
	
	//High is good
	private float NearestGhostDistanceBFS(GameState state) {
		class Pair {
			Point point;
			int distance;
			
			public Pair(Point point, int distance) {
				this.point = point;
				this.distance = distance;
			}
		}
		Maze.Status[][] map = state.map;
		int width = map.length;
		int height = map[0].length;
				
		Point pm = state.pacman;		
		
		GameState.GhostState[] ghosts = state.ghosts;
		
		Set<Point> visited = new HashSet<Point>();
		Queue<Pair> queue = new LinkedList<Pair>();
		queue.offer(new Pair(pm, 0));
		
		Point next = null;
		while(!queue.isEmpty())
		{
			Pair node = queue.poll();
			Point point = node.point;
			int distance = node.distance;
			
			for(GameState.GhostState ghost : ghosts) {
				if(point.equals(ghost.position)) return distance;
			}
		
			visited.add(point);
					
			//right
			next = new Point(wrap(point.x+1, width), point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//left
			next = new Point(wrap(point.x-1, width),point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
				
			//down
			next = new Point(point.x, wrap(point.y+1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//up
			next = new Point(point.x, wrap(point.y-1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
		}
		return 0;
	}
	
	//Sum of inverse ghost distances
	//High is bad
	private float SIDGhostBFS(GameState state, int maxDistance) {
		class Pair {
			Point point;
			int distance;
			
			public Pair(Point point, int distance) {
				this.point = point;
				this.distance = distance;
			}
		}
		
		Maze.Status[][] map = state.map;
		int width = map.length;
		int height = map[0].length;
		
		List<Point> ghosts = new ArrayList<Point>();
		for(GameState.GhostState ghost : state.ghosts) if(!ghost.isDead) ghosts.add(ghost.position);
		
		float total = 0;
		Set<Point> visited = new HashSet<Point>();
		Queue<Pair> queue = new LinkedList<Pair>();
			
		//distance is 1 if ghost is at pacman
		//cant use negative denominator
		queue.offer(new Pair(state.pacman, 1));
			
		Point next = null;
		while(!queue.isEmpty())
		{
			Pair node = queue.poll();
			Point point = node.point;
			int distance = node.distance;
			
			for(int i = 0; i < ghosts.size(); i++) {
				if(ghosts.remove(point)) {
					total += 1.0f / distance;
				}
			}
			
			if(ghosts.isEmpty()) {
				return total;
			}
			
			visited.add(point);
			
			if(distance >= maxDistance) continue;
			
			//right
			next = new Point(wrap(point.x+1, width), point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//left
			next = new Point(wrap(point.x-1, width),point.y);
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//down
			next = new Point(point.x, wrap(point.y+1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
			
			//up
			next = new Point(point.x, wrap(point.y-1, height));
			if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
				queue.offer(new Pair(next, distance + 1));
			}
		}
		return total;		
	}
	
	
	private int[][][][] getDistances(GameState initialState) {
		Maze.Status[][] map = initialState.map;
		int width = map.length;
		int height = map[0].length;
		Set<Point> dotPoints = new HashSet<Point>();
		for(int i = 0; i < width; i++){
			for(int j = 0; j < height; j++){
				if((map[i][j] == Maze.Status.DOT) || (map[i][j] == Maze.Status.ENERGISER)) dotPoints.add(new Point(i,j));
			}
		}
		int[][][][] distances = new int[28][36][28][36];
		
		for(Point dotPoint : dotPoints){
			Set<Point> visited = new HashSet<Point>();
			Queue<Point> queue = new LinkedList<Point>();
			queue.offer(dotPoint);
			int distanceCounter = 0;
			
			Point next = null;
			while(!queue.isEmpty())
			{
				Point node = queue.poll();
				distances[dotPoint.x][dotPoint.y][node.x][node.y] = distanceCounter;			
				visited.add(node);
						
				//right
				next = new Point(wrap(node.x+1, width), node.y);
				if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
					queue.offer(next);
				}
				
				//left
				next = new Point(wrap(node.x-1, width),node.y);
				if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
					queue.offer(next);
				}
					
				//down
				next = new Point(node.x, wrap(node.y+1, height));
				if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
					queue.offer(next);
				}
				
				//up
				next = new Point(node.x, wrap(node.y-1, height));
				if(map[next.x][next.y] != Maze.Status.DEAD && !visited.contains(next)) {
					queue.offer(next);
				}
				
				distanceCounter++;
			}
		}
		return distances;
	}
	
	//endregion
}
