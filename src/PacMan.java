import java.awt.Point;

public class PacMan {

	protected Mode 			mode;
	
	//position in screen coordinates
	protected Point 		originalPosition;
	//position in screen coordinates
	protected Point 		currentPosition;
	
	protected Orientation 	originalOrientation;
	protected Orientation 	currentOrientation;
	protected Orientation 	nextOrientation;
	
	//speed in screen coordinates
	protected int 			currentSpeed;
	protected PacManAgent 	agent;

	public enum Mode {
		CONTROLLED, AUTONOMOUS
	};

	public static final int[] SPEED = { 0, 1, 2, 4, 8, 16, 32 };

	/*
	 * The SPEED indicates the legal speed, representing how many pixels the
	 * center of Pac-Man or the ghost moves per frame. Same as the ghost speeds,
	 * they need to be divisors of MazeViewer.CELL_SIZE. The Orientation is used
	 * here to record the Pac-Man's moving direction as well.
	 */

	/*
	 * creates a Pac-Man with an initial position, and sets its initial
	 * orientation to Orientation.UP, mode to Mode.CONTROLLED, initial
	 * travelling speed to 16 (i.e. MazeViewer.CELL_SIZE).
	 */
	public PacMan(Point pos) {
		currentPosition = pos;
		currentOrientation = Orientation.UP;
		nextOrientation = currentOrientation;
		mode = Mode.AUTONOMOUS;
		setSpeed(MazeViewer.CELL_SIZE);
		originalPosition = currentPosition;
		originalOrientation = currentOrientation;
		//TODO change this for different agents
		agent = new MiniMaxAgent(this);
	}
	
	public PacMan(Point pos, GameState initialState) {
		currentPosition = pos;
		currentOrientation = Orientation.UP;
		nextOrientation = currentOrientation;
		mode = Mode.AUTONOMOUS;
		setSpeed(MazeViewer.CELL_SIZE);
		originalPosition = currentPosition;
		originalOrientation = currentOrientation;
		
		
		//TODO change this for different agents
		agent = new MiniMaxAgent(this, initialState);
	}

	public PacMan(Point pos, Orientation ori, int speed, Mode m) {
		currentPosition = pos;
		currentOrientation = ori;
		setMode(m);
		setSpeed(speed);
		nextOrientation = currentOrientation;

		originalPosition = currentPosition;
		originalOrientation = currentOrientation;
		agent = new ReflexAgent(this);
	}

	// region getter/setter

	public final boolean isDemo() {
		return mode == Mode.AUTONOMOUS;
	}

	public final int getSpeed() {
		// returns the travelling speed of Pac-Man.
		return currentSpeed;
	}

	public final void setSpeed(int newSpeed) {
		// changes the travelling speed to newSpeed.

		// if in speed array
		boolean found = false;
		for (int i = 0; i < SPEED.length; i++) {
			if (SPEED[i] == newSpeed) {
				found = true;
				break;
			}
		}

		if (found) {
			currentSpeed = newSpeed;
		} else {
			throw new IllegalArgumentException(
					"Error - Newspeed not in speed array");
		}
	}

	public final void setMode(Mode m) {
		// changes the mode to m. Again your code should ensure consistencies
		// between the input.
		// ??
		mode = m;
	}

	public final Orientation getOrientation() {
		// returns the current orientation of Pac-Man.
		return currentOrientation;
	}

	public final Point getPosition() {
		// returns the current position of Pac-Man.
		return currentPosition;
	}

	public final void setNextOrientation(Orientation ori) {
		nextOrientation = ori;
	}

	public final Orientation getNextOrientation() {
		return nextOrientation;
	}
	
	public final PacManAgent getAgent() {
		return agent;
	}
	// endregion

	public final boolean atGrid() {
		boolean canx = false;
		if ((currentPosition.x - MazeViewer.CELL_SIZE / 2)
				% MazeViewer.CELL_SIZE == 0)
			canx = true;
		boolean cany = false;
		if ((currentPosition.y - MazeViewer.CELL_SIZE / 2)
				% MazeViewer.CELL_SIZE == 0)
			cany = true;

		return canx && cany;
	}

	/**
	 * Sets nextOrientation to the closest dot if Autonomous then moves to
	 * nextOrientation at the next chance possible
	 * 
	 * @param maze
	 *            state of the maze
	 */
	public void doMove(Maze maze) {

		if (mode == Mode.AUTONOMOUS) {
			nextOrientation = agent.getNext(maze);
		}

		Maze.Status nextStatus = maze.locationStatus(nextPos(currentPosition,
				nextOrientation));
		boolean canturn = false;
		if (nextStatus == Maze.Status.DOT || nextStatus == Maze.Status.LEGAL
				|| nextStatus == Maze.Status.ENERGISER) {
			canturn = true;
		}

		// if on grid
		if (canturn && atGrid()) {
			move(maze, nextOrientation);
		} else {
			move(maze, currentOrientation); // keep going
		}
	}
	
	//region dont change
	//TODO
	//actually some of these arent very efficient
	/**
	 * moves Pac-Man in the maze if there is a legal tile in that direction ori,
	 * and change the orientation to ori. Processes
	 * 
	 * @param maze
	 * @param ori
	 */
	public final void move(Maze maze, Orientation ori) {
		boolean doMove = false;

		if (atGrid()) {
			switch (maze.locationStatus(nextPos(currentPosition, ori))) {
			case INVALID:
				// wrap around to other side of screen
				if (currentPosition.x == MazeViewer.CELL_SIZE / 2) { // left side
					currentPosition.x = maze.getMap().length
							* MazeViewer.CELL_SIZE - MazeViewer.CELL_SIZE / 2;

				} else if (currentPosition.x == maze.getMap().length
						* MazeViewer.CELL_SIZE - MazeViewer.CELL_SIZE / 2) { // right
																				// side
					currentPosition.x = MazeViewer.CELL_SIZE / 2;
				}

				break;

			case DEAD:
				// don't move
				maze.doStopDot(); // no more dot sounds
				break;

			case ENERGISER:
				// eat energiser
				// add 50 score
				maze.doEnergiser();
				maze.setStatus(nextPos(currentPosition, ori).x / MazeViewer.CELL_SIZE,
							   nextPos(currentPosition, ori).y / MazeViewer.CELL_SIZE,
							   Maze.Status.LEGAL);
				// panic!

				Ghost.setPanic(maze.getGhosts(), true);
				maze.doPanicCountdown();

				doMove = true;
				break;

			case DOT:
				// eat dot
				// add 10 score
				maze.doDot();
				maze.setStatus(nextPos(currentPosition, ori).x
						/ MazeViewer.CELL_SIZE, nextPos(currentPosition, ori).y
						/ MazeViewer.CELL_SIZE, Maze.Status.LEGAL);
				doMove = true;
				break;

			case LEGAL:
				// move
				doMove = true;
				maze.doStopDot();
			}
		} else {
			doMove = true;
		}

		if (doMove) {

			currentPosition = nextPos2(currentPosition, ori); // try for
															// smoothness
			currentOrientation = ori;

		}
	}

	/**
	 * Returns a translated clone of a Point in the given direction one grid
	 * space away. (in screen coordinates!)
	 * 
	 * @param position
	 *            Point needed to be translated position
	 * @param ori
	 *            orientation that will be moved
	 * @return
	 */
	protected static final Point nextPos(Point position, Orientation ori) {
		Point positioncopy = new Point(position);
		int dist = MazeViewer.CELL_SIZE;
		switch (ori) {
		case UP:
			positioncopy.translate(0, -dist);
			break;
		case DOWN:
			positioncopy.translate(0, dist);
			break;
		case LEFT:
			positioncopy.translate(-dist, 0);
			break;
		case RIGHT:
			positioncopy.translate(dist, 0);
			break;
		}
		return positioncopy;
	}

	/**
	 * Returns a translated clone of a Point in the given direction at pacman's
	 * speed.
	 * 
	 * @param position
	 *            Point needed to be translated position
	 * @param ori
	 *            orientation that will be moved
	 * @return
	 */
	private final Point nextPos2(Point position, Orientation ori) {
		Point positioncopy = new Point(position); // translate copy of point
		int dist = (currentSpeed / MazeViewer.CELL_SIZE) * 2;
		switch (ori) {
		case UP:
			positioncopy.translate(0, -dist);
			break;
		case DOWN:
			positioncopy.translate(0, dist);
			break;
		case LEFT:
			positioncopy.translate(-dist, 0);
			break;
		case RIGHT:
			positioncopy.translate(dist, 0);
			break;
		}
		return positioncopy;
	}

	public final void doDemo() {
		if (mode == Mode.AUTONOMOUS) {
			mode = Mode.CONTROLLED;
		} else {
			mode = Mode.AUTONOMOUS;
		}
	}


	public final void setToOriginal() {
		currentPosition = originalPosition;
		currentOrientation = originalOrientation;
	}
	//endregion
}
