\documentclass[a4paper]{article}
\usepackage[cm]{fullpage}
\usepackage{amsmath}
\begin{document}
\title{Autonomous Pac-Man}
\author{Callan Gray (21341958), James Udiljak (21294169)}
\date{}
\maketitle

\section{Introduction}
Pac-Man is classic arcade video game developed in the late 70s and released to international audiences in the early 80s. The premise of the game is simple: using a four way joystick, the player must navigate through a maze collecting dots to gain points towards a high score. Whilst this happens, four ghosts starting from the centre also navigate through the maze trying to chase Pac-Man and take his lives. In addition to the regular dots scattered throughout the maze, there are four large dots called energisers that will temporarily put ghosts into panic mode, both slowing them and making them able to be eaten by Pac-Man for bonus points.

The purpose of this project is to design artificial intelligence for Pac-Man which will try to achieve the best score possible.

\section{Problem Identification}
\subsection{Sensors and Actuators}
To plan the behaviour of an intelligent agent, we need to understand how the agent can read its environment through its sensors, and how it can then perform actions in response through actuators. In the case of AI controlling Pac-Man, this can be somewhat straightforward by inferring that Pac-Man should be restrained to act with similar if not the exact same sensors and actuators that a human player has. The sensors which were identified and considered for the development of the following algorithms include:

\begin{itemize}
  \item The absolute position and orientation of Pac-Man, ghosts, dots, and energisers. The screen never moves with respect to the maze, and each of these elements always remains visible.
  \item The exact time that Pac-Man stays energised.
  \item The number of points which dots, energisers and ghosts are worth when they are eaten.
  \item The player's current score.
\end{itemize}

In addition to these sensors, a player or agent could also infer a number of other not very well known rules of the game through experience. This include things such as:
\begin{itemize}
  \item ghosts are unable to turn around whilst they navigate the maze, even at the junctions they come across.
  \item all ghosts must completely turn around when Pac-Man eats an energiser. 
  \item each consecutive ghost eaten while energized will increase the amount of bonus points of the next one.
  \item it is possible to eat more than four ghosts in a row if Pac-Man eats another energiser while energized.
\end{itemize}

Whilst it is technically possible within the code to read and use a number of other resources, such as the actual ghost artificial intelligence, we will consider this cheating, even though a human, given enough time, could completely reverse engineer such knowledge of the game's mechanics. We have confined ourselves to the rule that complex future predictions about the ghosts should be developed using knowledge of only the above observations, and any runtime learning algorithms which we implement.\newline

Pac-Man has only a single actuator, which is his preferred direction of movement for the next frame. Unlike the ghosts, Pac-Man is capable of turning around 180 degrees and can stop only when he is facing a wall which sits directly in front of him. He moves at a constant speed throughout the duration of the game.\newline

\section{Algorithm 1: Simple Agent}
The first algorithm which was developed does not utilise any lookahead, that is evaluating future states of the game, but instead evaluates the existing state with high complexity heuristic functions similar to how a typical human player might approach playing the game.

\subsection{Implementation}
The algorithm begins by deciding between three possible objectives:
\begin{itemize}
  \item Search for the nearest dot and eat it.
  \item Run away from nearby ghosts.
  \item Chase and eat ghosts when energized.
\end{itemize}

Determining which objective to aim for is decided by simply using two variables calculated from Pac-Man's sensors: the distance to the nearest ghost, and a boolean value denoting whether or not Pac-Man is energised. If Pac-Man is not energised and a ghost is within a certain distance, flee, else if Pac-Man is energised and a ghost is within a certain distance, chase it. Otherwise, Pac-Man should continue with his primary objective, which is collecting the dots scattered about the maze.\newline

\subsubsection{Search mode}
In search mode, a breadth first search (originally used by its inventor Edward F. Moore to find the shortest path out of a maze) is used to explore a gridspace representation of the maze, which is done in order to simplify the search algorithm and increase performance, but does involve rounding PacMan and ghost positions to the nearest grid square. This uninformed search algorithm is only performed once per turn is guarenteed to find closest dot and traces the direction that leads to the dot it finds. This algorithm works very well and does not result in getting caught in local maxima. An A* implementation of this was considered as the search would perform better on average, but would require knowing which dot is the destination before the search.\newline

\subsubsection{Flee mode}
In flee mode Pac-Man chooses the move that results in the lowest value obtained from weighing two utility functions for each possible grid square he can move to next. The sum of inverse distances to each ghost is essentially a utility that measures the 'closeness' of the ghosts. This heuristic was found in testing to be more effective at evasion than any other tested, especially in situations where Pac-Man was trying to evade multiple ghosts from different directions, as the inverse distance drops at the highest rate when Pac-Man moves away from the closest ghost. Essentially, the heuristic is 'dominated' by the closest ghost, without neglecting the others. This was implemented using a single depth limited breadth first in grid space. A single breadth first search ensures that grid nodes are not searched more than once, and is depth limited due to the fact that the inverse of larger a distance will result in a smaller value which can be considered almost negligible. Using this utility alone however results in PacMan completely ignoring dots when trying to escape from ghosts, thus a second utility, inverse nearest dot distance, is calculated and added onto the first utility for each direction to effectively consider moving towards a dot if two escape routes are found.\newline

\subsubsection{Chase mode}
Chase mode is only activated when Pac-Man is under the effects of an energizer and a ghost is within a particular range. Here, an A* search is used to chase the ghost with the shortest Manhattan distance to Pac-Man. Otherwise this mode defers to search mode, and Pac-Man resumes eating pellets. The A* search uses the Manhattan distance between the ghost and Pac-Man as its heuristic. This heuristic is both admissible and consistent.

\subsection{Performance}
Performance of the algorithm was found to be quite reasonable. It was rare to see Pac-Man be eaten by the ghosts, even when seemingly surrounded with no escape. Pac-Man also completed the level with relative speed. 

\subsubsection{Pros}
It was rare to see Pac-Man let the dots become segmented into isolated regions, forcing him to waste time traversing back and forth across empty regions of the maze. Instead he seemed to methodically sweep across the maze, disturbed only by attacking ghosts. This behaviour is most likely attributed to the fact that Pac-Man would always seek the nearest dot first when in search mode.\newline

Local maxima were not a problem here, as Pac-Man never changed his dot target unless he had to flee or chase ghosts, so there was no way for him to be caught indecisive between two pellets. This meant that no tiebreaking heuristics were required.

\subsubsection{Cons}
We observed slightly odd pathfinding behaviour when Pac-Man was near the maze's two wraparound points. Pac-Man seemed to wander somewhat aimlessly if him and one or more ghosts were near to one of these wraparound points.\newline

There was also a tendency for him to pace back and forth when a ghost was patrolling a circuit which he wished to traverse in order to get to the nearest dot. This was not a maxima issue, as the loop was more than two steps long. Pac-Man would enter the ghost's circuit in search mode, when the ghost was on the far side of the circuit, then, as the ghost made its way around to the near side of its circuit, Pac-Man would retreat back to where he came from in 'flee' mode. This process would repeat until a second ghost came along and 'jolted' the AI out of its loop, or the ghosts decided to launch a 4 ghost attack. Here a reinforcement learning algorithm would be beneficial: The path returned by search mode's breadth first search would be considered by the AI's reinforcement learning model as an historically poor choice, and the agent would instead suggest a different path, breaking from the loop.\newline

\section{Algorithm 2: MiniMax}
This algorithm utilizes similar heuristics to the first, but combines them into a single utility which is used to find the best move via a MiniMax tree search with alpha beta pruning. An important part of making this algorithm work well was 'tuning' the balance of each heuristic by finding weightings to apply to them before they are summed. The weightings should make sure that each heuristic 'dominates' when it is supposed to and remains passive otherwise. For example, when Pac-Man is in danger of being eaten by a ghost, the heuristic which helps Pac-Man avoid ghosts should dominate the utility. When Pac-Man is not in danger, however, the ghost avoidance heuristic's effect on Pac-Man's move choice should be negligible, so as not to slow down Pac-Man in his pursuit of other goals unnecessarily.

\subsection{Implementation}
The algorithm is a standard recursive minimax algorithm with alpha beta pruning, eschewing the 'mode' system of the simple agent and using appropriate heuristic weightings in order to 'naturally' determine AI behavioural traits. Instead of moving each ghost in turn and having a 'miniminiminiminimax' algorithm, we opted to combine all ghost moves into a single minimizing move, especially since the branching factor for most of these moves was quite low.

In order to implement minimax, the agent must have a solid understanding of the rules of Pac-Man in order to predict future states for it to evaluate. This means that we must be able to accurately describe all possible future scenarios of the game to a certain minimax depth. This was done by creating a new class that stores all relevant information about the state of the game, and breaks the game down into a turned based simulation. Again, rounding to grid squares is used here to improve performance and assumes that Pac-Man and the ghosts take turns in moving one grid square at a time.

\subsubsection{Weighted dot density}
This heuristic sums the inverse minimum distances (these are the actual minimum distances that one must walk through the maze, not Manhattan distances) from Pac-Man to every uneaten dot in the maze at any given moment, using a precalcuated 4D array of minimum distances to each dot from each of the grid squares, which is generated at startup using a series of breadth first searches. These inverse distances are artificially skewed so that Pac-Man prefers to eat dots from the bottom right hand corner of the map. This heuristic is not usually the dominant heuristic driving pellet collection, however. It is simply used as a tiebreaking mechanism to help prevent Pac-Man from getting stuck in a local maximum. It also assists in encouraging Pac-Man to seek clumps of dots, rather than singular dots. However, in the late game, where it is likely that Pac-Man's lookahead allowance will end before he 'forsees' himself eating a dot, this heuristic does dominate pellet seeking behaviour.

\subsubsection{How many dots have I eaten?}
This simple heuristic returns the number of dots that Pac-Man has eaten in a given state. It is the dominant heuristic when Pac-Man is not under threat by a ghost, and is the main engine which drives Pac-Man to collect the pellets around the map.

\subsubsection{Sum of inverse ghost distances}
This uses a depth limited breadth first search to find nearby ghosts and sum their inverse distances to Pac-Man in the current game state. By summing the inverse distances, and then taking the inverse of that sum, we obtain an heuristic which is higher when we are further away from the ghosts, as we want it to be, because Pac-Man is the maximizing player in our minimax algorithm. The heuristic is dominated by the closest ghost but also takes into account the others, allowing very good ghost evasion even when Pac-Man is seemingly surrounded. \newline

\subsubsection{Combination of heuristics}
The heuristics are combined in the following way to generate the final utility values, which the minimax algorithm uses to compare states:\newline

\( U = c_1 p + c_2 d + \frac{c_3}{\sqrt{g+c_4}}\)\newline

Here, \( U\) is the final utility, \( p\) is the weighted dot density, \( d\) is the number of dots eaten, \( g\) is the summed, depth limited inverse ghost distance and the \( c_i \) are constants. The inverse square root of \( g\) is taken in order to make the ghost distance heuristic's influence on Pac-Man's movement fall off more quickly as the ghosts get further away, so that it is able to dominate while Pac-Man is in danger, but gives way to heuristics that encourage pellet collection otherwise. The constant \( c_4\) is used to ensure that the division never results in an infinite utility value. The constants, along with the depth limit for the sum inverse ghost distance breadth first search, were tuned to obtain the best balance between ghost evasion and pellet seeking. Setting the depth limit too low for the breadth first search would cause Pac-Man to easily become outmaneuvered when pursued by multiple ghosts. Setting the depth limit too high would cause Pac-Man to be too cautious of ghosts, and avoid pellets that could be safely gathered.

\subsection{Performance}
Performance of the algorithm scaled as expected with depth allowance for each AI step. As the depth allowance increased, so did Pac-Man's evasive abilities. Pellet collection speed was however largely unchanged, as seeking a stationary object does not benefit from lookahead (i.e. we gain no knowledge from computing all possible pellet 'moves' because the pellets do not move). With careful tweaking of the utility constants, Pac-Man was achieving completion times equal to or better than those of the simple agent. The real difference was in the failure rate. It was much rarer to see Pac-Man eaten by the ghosts with the minimax algorithm.

\subsubsection{Pros}
Minimax, being a more sophisticated algorithm, naturally allowed Pac-Man to have superior evasion abilities. The lookahead enabled Pac-Man to see situations where he would be surrounded before they eventuated, and escape through a nearby junction. Naturally, the longer the lookahead, the better the evasion capabilities. In an environment with infinite computational power and memory Pac-Man may well be able to choose moves such that he never has to give up a dot and come back to it later in order to avoid a ghost, however this is just a thought experiment, as such an environment does not exist. \newline

Pac-Man also benefitted greatly from the ability to consider all possible goals at once (such as eating scared ghosts and eating pellets), instead of choosing a single goal to focus on at any one moment. This meant he was able to intelligently choose strategies that consolidated his rewards (choose the square that has both a scared ghost and a pellet on it, instead of the squares with only one of the two), allowing him to save time in completing the level.\newline

The fact that ghost AI never makes 180 degree turns makes the branching factor of the MiniMax tree very small, as this means that a branch can only occur on a ghost move when a ghost reaches a junction in the maze. Typically each branch point is binary, or at most tertiary, as all junctions are either T junctions or four way junctions, and the entrance is not a valid exit as the ghosts cannot make 180 degree turns. This allows for far reaching lookahead in a relatively small amount of time.\newline

For the original design of the simple agent we considered an heuristic that would prioritize dots which are part of long continuous runs of dots over dots who are fairly isolated, in order to prevent a situation where Pac-Man might target a single dot simply because it is closest, when it may have been a better strategy to go for the snake of 10 dots that is only one block further away. It is clear that such a heuristic is a natural corollary of both the weighted dot density heuristic and the minimax algorithm itself, as lookahead would see that gathering one dot one turn sooner is worse than gathering 10 dots, beginning one turn later. This is a big positive for minimax over the simple algorithm, it helps to avoid wasting time traversing empty space. In the shortest possible game, Pac-Man will spend very little time traversing areas of the maze which do not have dots in them. However, some backtracking is required, the layout of the maze does not allow a complete circuit which only traverses each grid square once.\newline

The dot density heuristic was superior to the closest dot heuristic from the simple algorithm, as it allows Pac-Man to prioritize groups of dots that are further away over single dots that are close, avoiding segmentation of the dots in the maze, and reducing backtracking. The weighting of the dot density heuristic which makes Pac-Man prioritize dots in a specific quadrant of the map helped to get Pac-Man to sweep smoothly from one end of the map to the other, again assisting in avoiding segmentation of the dots and thus keeping level completion times low.

\subsubsection{Cons}

It was noted that, beyond a certain point, increasing depth did not increase the performance of the algorithm. This is because the algorithm assumed the ghosts were smarter than they are in actuality. The algorithm works on the assumption that the ghosts are always chasing Pac-Man when determining ghost moves, but the default ghost AI is often quite happy to simply patrol the maze aimlessly, or 'defend' a small portion of the maze by circling around a block, especially when Pac-Man is not nearby. Thus it artificially inflates the probability that Pac-Man will become near a ghost in the late part of the lookahead by assuming that all ghosts constantly seek him, even from the far side of the maze. The effect of this is that Pac-Man is more timid when he needn't be with large lookahead values. A reinforcement learning algorithm would solve this problem more organically, perhaps being intially timid but learning to test the boundaries of just what is a safe distance to a ghost over time. A sophisticated enough algorithm could even learn to recognize when the ghosts are in patrol mode and when they are in attack mode, becoming more aggressive while the ghosts are timid and less likely to pursue Pac-Man. \newline

The hardest part of implementing this algorithm was finding a utility function that avoided local maxima as much as possible. A lot of weighting tuning was required to resolve conflicts between two heuristics that would cause them to 'fight' against each other for control of Pac-Man. Some of the individual heuristics themselves exhibited local maxima, and we even implemented tiebreaking systems into our utility in order to combat these. A reinforcement learning algorithm would very quickly recognize the moves in the local maximum loop as historically poor, and instead offer a different move, acting as its own tiebreaking system and avoiding local maxima.

\subsection{Possible further heuristics}

A heuristic that may be useful would be one which places extra value on Pac-Man being at or near a junction square when he is being pursued by one or more ghosts. With the right weightings this could allow him to better evade multiple targets, as junctions in the maze act as the main 'escape' mechanism from a multiple ghost assault. Of course, if this were poorly tuned it would result in a local maximum condition whereby Pac-Man would wait around at a junction square until ghosts were surrounding him from all sides, and he no longer had any chance of escape.\newline

Another heuristic that may prove useful would be one which places extra value on Pac-Man eating energisers when he is being pursued by ghosts, and even diminishing the energiser square value when Pac-Man is not in danger, in order to save them for later. Of course, if we had lots of computing power, minimax might organically determine that it is better to leave an energiser uneaten unless Pac-Man is in danger, if far lookahead were to examine a situation in which Pac-Man left the energiser for later, but while we don't have the computing power to see this far ahead into the game it may be useful to approximate this behaviour with an heuristic that implies the strategy of leaving energisers until they are urgently needed.

\end{document}